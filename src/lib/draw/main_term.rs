#[allow(dead_code)]

use std::{error::Error, io};
use termion::cursor;


use termion::{event::*, input::MouseTerminal, raw::{IntoRawMode, RawTerminal}, screen::AlternateScreen};
use tui::{Terminal, backend::TermionBackend, layout::{Alignment, Constraint, Corner, Direction, Layout}, style::{Color, Modifier, Style}, text::{Span, Spans}, widgets::{Block, Borders, List, ListItem, Paragraph, BorderType}};

use crate::lib::draw::util::event::{TEvent, Events};

use super::util::StatefulList;


pub fn open_term() -> Result<Terminal<TermionBackend<RawTerminal<io::Stdout>>>, io::Error> {

    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.draw(|f| {
        let size = f.size();
        let block = Block::default()
            .title("Block")
            .borders(Borders::ALL);
        f.render_widget(block, size);
    })?;
    Ok(terminal)
}

/// App holds the state of the application
struct App<'a> {

    items: StatefulList<(&'a str, usize)>,

}

impl<'a> App<'a> {
    fn default() -> App<'a> {
        App {
            items: StatefulList::with_items(vec![
                ("1 - New match", 1),
                ("2 - Import team data", 1),
                ("3 - Init match", 1),
                ("4 - Exit", 1),
            ])
        }
    }
}

pub fn main_menu_term() -> Result<usize, Box<dyn Error>> {

    // Initialize terminal
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    let mut stdout = MouseTerminal::from(io::stdout().into_raw_mode().unwrap());


    let stdin = io::stdin();


    // Setup event handlers
    let events = Events::new();

    // Create default app state
    let mut app = App::default();

    

    loop {

        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints(
                    [
                        Constraint::Percentage(15),
                        Constraint::Percentage(83),
                        Constraint::Percentage(1),
                    ]
                    .as_ref(),
                )
                .split(f.size());

            let title_block = Block::default()
                .title("")
                .title_alignment(Alignment::Center)
                .border_type(BorderType::Rounded)
                .borders(Borders::ALL);
            
            let text = vec![
                Spans::from("Welcome to the VolleyBall Marker app"),
            ];

            let paragraph = Paragraph::new(text)
                .style(Style::default().fg(Color::White))
                .block(title_block)
                .alignment(Alignment::Center);
            f.render_widget(paragraph, chunks[0]);
            
            let footer_chunks = Layout::default()
            .direction(Direction::Horizontal)
            .constraints(
                [
                    Constraint::Percentage(10),
                    Constraint::Percentage(80),
                    Constraint::Percentage(10),
                ]
                .as_ref(),
            )
            .split(chunks[2]);
           
            let version_paragraph = Paragraph::new(vec![Spans::from("v0.0.1")])
                .style(Style::default().fg(Color::White))
                .alignment(Alignment::Left);
            f.render_widget(version_paragraph, footer_chunks[0]);
            

            let footer_paragraph = Paragraph::new(vec![Spans::from("Maofryy")])
                .style(Style::default().fg(Color::White))
                .alignment(Alignment::Right);
            f.render_widget(footer_paragraph, footer_chunks[2]);
           
            //No need for block, juste a layout with Paragraph 20 , List 20 , Empty 20 
            // First paragraph With "Select one and press enter"
            // Second is a list with line highlighting, thats changes when moving
            // Third is juste an empty space to give room to the page
            // Main menu should be done, need to get back the answer to the main function after 
            let content_chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Ratio(1, 4),
                    Constraint::Ratio(2, 4),
                    Constraint::Ratio(1, 4),
                ]
                .as_ref(),
            )
            .horizontal_margin(f.size().width/3)
            .vertical_margin(f.size().height/12)
            .split(chunks[1]);


            let title_paragraph = Paragraph::new(vec![Spans::from("Select one and press 'ENTER' : ")])
                .style(Style::default().fg(Color::White))
                .alignment(Alignment::Center);
            f.render_widget(title_paragraph, content_chunks[0]);

            /*
            let item_chunk = Layout::default()
            .direction(Direction::Vertical)
            .constraints(
                [
                    Constraint::Percentage(100),
                ]
                .as_ref(),
            )
            .vertical_margin(4)
            .split(content_chunks[1]);
            */


            // Items preparation
            let items: Vec<ListItem> = app
                .items
                .items
                .iter()
                .map(|i| {
                    let lines = vec![
                        Spans::from(Span::styled(i.0, Style::default().add_modifier(Modifier::BOLD)))
                    ];
                    ListItem::new(lines).style(Style::default().fg(Color::White)) 
                })
                .collect();


            let items = List::new(items)
                .block(Block::default().borders(Borders::NONE).title(""))
                .highlight_style(
                    Style::default()
                        .bg(Color::DarkGray)
                        .add_modifier(Modifier::BOLD),
                )
                .start_corner(Corner::TopLeft)
                .highlight_symbol(" => ");

            // We can now render the item list
            f.render_stateful_widget(items, content_chunks[1], &mut app.items.state);

            // f.render_widget(items, content_chunks[1]);


        })?;


        // Keyboard controls
        if let TEvent::Input(input) = events.next()? {
            TEvent::Input(input) => match input {
                Key::Char('q') => {
                    return Ok(4);
                }
                Key::Left => {
                    app.items.unselect();
                }
                Key::Down => {
                    app.items.next();
                }
                Key::Up => {
                    app.items.previous();
                }
                Key::Char('\t') => {
                    app.items.next();
                }
                Key::BackTab => {
                    app.items.previous();
                }

                Key::Char('\n') => {
                    match app.items.state.selected() {
                        Some(x) => return Ok(x+1),
                        None => (),
                    } 
                    
                }
                Key::Char('1') => return Ok(1),
                Key::Char('2') => return Ok(2),
                Key::Char('3') => return Ok(3),
                Key::Char('4') => return Ok(4),
                _ => {
                    println!("Key = {:?}", input);
                }
            },
            TEvent::Mouse(me) => {
                match me {
                    MouseEvent::Press(_, 1, 1) => {
                        write!(stdout, "1, 1");
                    },
                    MouseEvent::Press(_, a, b) |
                    MouseEvent::Release(a, b) |
                    MouseEvent::Hold(a, b) => {
                        write!(stdout, "{}", cursor::Goto(a, b)).unwrap();
                    }
                }
            },
            _ => (),
        } 
    }
}