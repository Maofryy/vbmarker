use std::fmt;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize)]
pub struct Player {
    pub num     : u8,
    pub pos     : u8,
    pub role    : Role,
}

impl Player {

    pub fn new() -> Player {
        Player {
            num: 0,
            pos: 0,
            role: Role::Complet,
        }
    }

    pub fn rotate(&mut self){
        if self.pos == 6 {
            self.set_pos(1);
        } else if self.pos != 0 {
            self.set_pos(self.pos() + 1);
        }
    }

    pub fn to_court(&mut self, pos: u8) {
        self.set_pos(pos);
    }

    pub fn to_bench(&mut self) {
        self.set_pos(0);
    }

    /// Set the player's pos.
    pub fn set_pos(&mut self, pos: u8) {
        self.pos = pos;
    }

    /// Get a mutable reference to the player's pos.
    pub fn pos_mut(&mut self) -> &mut u8 {
        &mut self.pos
    }

    /// Get a reference to the player's pos.
    pub fn pos(&self) -> u8 {
        self.pos
    }
}

impl fmt::Display for Player {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {})", self.num, self.pos, self.role)
    }
}

#[derive(Serialize, Deserialize)]
pub enum Role {
    Setter,
    Opposite,
    Outside,
    Middle,
    Libero,
    Complet,
}

impl fmt::Display for Role {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let print = match *self {
            Role::Setter => "Setter",
            Role::Opposite => "Opposite",
            Role::Outside => "Outside",
            Role::Middle => "Middle",
            Role::Libero => "Libéro",
            Role::Complet => "Complet",
        };
        write!(f, "{}", print)
    }
}