use std::fmt;
use crate::lib::object::score::*;
use crate::lib::object::team::*;
use serde::{Serialize, Deserialize};

pub enum Position {
    BENCH,
    I,
    II,
    III,
    IV,
    V,
    VI,
}

#[derive(Serialize, Deserialize)]
pub struct Match {
    pub location    : &'static str,
    pub team1       : Team,
    pub team2       : Team,
    pub score       : Score,
    pub set_number  : usize,
    pub referee     : &'static str, 
}

impl fmt::Display for Match {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Match at {}\t Referee : {}\n", self.location, self.referee)?;
        write!(f, "Team 1 :\n{}", self.team1)?;
        write!(f, "Team 2 :\n{}", self.team2)?;
        //write!(f, "Score :\n{}", self.score)?;
        Ok(())
    }
}

impl Match {
    pub fn new() -> Match {
        Match {
            location : "",
            team1    : Team::new(),
            team2    : Team::new(),
            referee  : "",
            score    : Score::new(),
            set_number : 0,
        }
    }

    pub fn match_won(&self) -> u8 {
        let mut set_score_1: u8 = 0;
        let mut set_score_2: u8 = 0;

        for set_number in 0..=5 {
            match self.score.set_won(set_number) {
                1 => set_score_1 += 1,
                2 => set_score_2 += 1,
                _ => (),
            }
        }

        if set_score_1 >= 3 && set_score_1 >= set_score_2 + 2 {
            return 1;
        } else if set_score_2 >= 3 && set_score_2 >= set_score_1 + 2 {
            return 2;
        } else {
            return 0;
        }
    }

    pub fn add_point(&mut self, team_number : u8){
        match team_number {
            1 => self.score.add_point(self.set_number, 0),
            2 => self.score.add_point(self.set_number, 1),
            _ => panic!("Wrong team number."),
        }
        match self.score.set_won(self.set_number) {
            1 => {println!("Team 1 won the set."); self.set_number += 1},
            2 => {println!("Team 2 won the set."); self.set_number += 1},
            _ => (),
        }
    }

    pub fn sub_player(&mut self, team1 : usize, on_court: u8, on_bench: u8) -> bool {
        // Teams have max 6 mut per set but change backs doesnt count :X
        if self.score.can_team_sub(self.set_number, team1, on_court, on_bench) {
            match team1 {
                0 => {self.team1.sub(on_court, on_bench); return true;},
                1 => {self.team2.sub(on_court, on_bench); return true;},
                _ => {println!("Wrong team number."); return false},
            }
        } else {
            return false;
        }
    }
}

