
use serde::{Serialize, Deserialize};
use bit_vec::BitVec;

#[derive(Serialize, Deserialize)]
pub struct Score {
    pub set : [[u8;5];2],
    pub point_history : [BitVec; 5],
    pub point_index : [usize; 5],
    pub timeout : [[u8;5];2],
    pub subs : [[u8;5];2],
    pub active_subs : [[[(u8, u8);6];5];2],
}

impl Score {
    pub fn new() -> Score {
        //TODO: FIND A WAY TO STORE HISTORY OF POINTS
        //? Binary map of who takes the point ? and we're able to keep track of 


        Score {
            set : [[0; 5];2],
            point_history : [
                BitVec::from_elem(100, false),
                BitVec::from_elem(100, false),
                BitVec::from_elem(100, false),
                BitVec::from_elem(100, false),
                BitVec::from_elem(100, false),
            ],
            point_index : [0; 5],
            timeout : [[2; 5];2],
            subs : [[6; 5];2],
            active_subs : [[[(0, 0);6];5];2],
            //? binary list/hash/map of which teams takes which point ?
        }
    }

    pub fn add_point(&mut self, set_number : usize, team1 : usize){
        self.set[team1][set_number] += 1;
        let res = if team1 == 1 { true } else { false };
        self.point_history[set_number].set(self.point_index[set_number], res);
        self.point_index[set_number] += 1;
    }
    
    pub fn sub_point(&mut self, set_number : usize, team1 : usize){
        self.set[team1][set_number] -= 1;
        let res = if team1 == 1 { true } else { false };
        self.point_index[set_number] -= 1;
        self.point_history[set_number].set(self.point_index[set_number], res);
    }

    // Check after assigning point
    pub fn serve_change(&self, set_number : usize) -> bool {
        //return if the value for the current point in history is the same as the previous point
        if self.point_index[set_number] == 0 {
            return false;
        }
        let current =self.point_history[set_number].get(self.point_index[set_number]).unwrap();
        let previous =self.point_history[set_number].get(self.point_index[set_number-1]).unwrap();

        return current ^ previous; 
    }

    /// Check if the set given in arg is finished and gives which team won:
    /// Return values :
    ///      0 => No team won
    ///      1 => Team 1 won
    ///      2 => Team 2 won
    pub fn set_won(&self, set_number : usize) -> u8 {
        //? DONE: Need to handle 2 points difference
        
        let mut set_length : u8 = 25;
        if set_number == 5 {
            set_length = 15;
        }
        if  self.set[0][set_number] >= set_length && 
            self.set[0][set_number] >= (self.set[0][set_number] + 2){
            return 1;
        } else if self.set[0][set_number] >= set_length && 
            self.set[0][set_number] >= (self.set[0][set_number] + 2 ){
            return 2;
        } else {
            return 0;
        }
    }


    pub fn timeout_team(&mut self, set_number : usize, team1: usize) -> bool {
        if self.timeout[team1][set_number] > 0 {
            println!("Time out team 1");
            self.timeout[team1][set_number] -= 1;
            return true;
        } else {
            println!("No more time out for team 1 this set.");
            return false;
        }
    }

    pub fn is_an_active_sub(&self, set_number : usize, team1 : usize, on_court: u8, on_bench: u8) -> bool {
        match self.active_subs[team1][set_number].iter().find(|&x| x.0 == on_bench && x.1 == on_court) {
            Some(_) => return true,
            None => return false,
        }
    }

    pub fn add_active_sub(&mut self, set_number : usize, team1 : usize, on_court: u8, on_bench: u8) {
        //find the first empty tuple for the set
        //if (team1) {
            match self.active_subs[team1][set_number].iter_mut().find(|x| x.0 == 0 && x.1 == 0) {
                Some(x) => {
                    x.0 = on_court;
                    x.1 = on_bench;
                }
                None => (),
            }
        //}
    }

    pub fn can_team_sub(&mut self, set_number : usize, team1: usize, on_court: u8, on_bench: u8) -> bool {
        //? Follow the pseudo code : 
        
        //* Is the sub an active sub ?
        if self.is_an_active_sub(set_number, team1, on_court, on_bench){
            return true
        } else {
            if self.subs[team1][set_number] > 0 {
                self.subs[team1][set_number] -= 1;
                self.add_active_sub(set_number, team1, on_court, on_bench);
                return true
            } else {
                return false;
            }
        }
    }
    
    /* 
    /// Set the score's set team 1.
    pub fn set_set_team_1(&mut self, set_team_1: [u8;5]) {
        self.set[team1] = set_team_1;
    }

    /// Set the score's set team 2.
    pub fn set_set_team_2(&mut self, set_team_2: [u8;5]) {
        self.set_team_2 = set_team_2;
    }

    /// Get a reference to the score's set team 2.
    pub fn set_team_2(&self) -> &[u8;5] {
        &self.set_team_2
    }

    /// Get a mutable reference to the score's set team 2.
    pub fn set_team_2_mut(&mut self) -> &mut [u8;5] {
        &mut self.set_team_2
    }

    /// Get a reference to the score's set team 1.
    pub fn set_team_1(&self) -> &[u8;5] {
        &self.set_team_1
    }

    /// Get a mutable reference to the score's set team 1.
    pub fn set_team_1_mut(&mut self) -> &mut [u8;5] {
        &mut self.set_team_1
    }
    */

}
