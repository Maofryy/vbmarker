var utils = new x_lof_pbd.PBDProjectTeam();

var project_not_updated = new GlideRecord('x_lof_pbd_project');

var myQuery = "sys_class_name=pm_project^stateNOT IN3,4,7^sys_updated_on<javascript:gs.beginningOfLastWeek()";

project_not_updated.addEncodedQuery(myQuery);
project_not_updated.query();

var projectTeam = [];

while (project_not_updated.next()){
    var project_id = project_not_updated.getUniqueValue();
    gs.info("project_not_update " + project_not_updated);
    var json_projectTeam = utils.getProjectTeam(project_not_updated.getUniqueValue());

    for (var user in json_projectTeam) {
        var user_id = json_projectTeam[user].sys_id;
        if (!projectTeam[user_id])
            projectTeam[user_id] = [];
        if (projectTeam[user_id].indexOf(project_id) == -1) {
            projectTeam[user_id].push(project_id);
        }
    }
}

// Now for each person concerned we send an email with the list of open projects with nor recent activity

for (var p_sys_id in projectTeam){
    gs.info("P_sys_id " + p_sys_id);
    if (projecteTeam[p_sys_id].length != 0){
        var projects = projectTeam[p_sys_id].toString();
        //gs.eventQueue('x_lof_pbd.weekly_project_team', null, p_sys_id, projects);
        gs.info("id = " + p_sys_id+ ", " + projects);
    }
}