
use std::{fmt::{self, write}, error::Error};
use std::io;
use crate::lib::object::player::*;
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct Team {
    pub name    : &'static str,
    pub coach   : &'static str,
    pub roster  : Vec::<Player>,
    pub players : [u8;6],        
}

impl Team {
    pub fn new() -> Team {
        Team {
            name    : "",
            roster  : Vec::<Player>::new(), 
            coach   : "",
            players : [0;6],
        }
    }

    pub fn is_player(&mut self, num: &u8) -> bool {
        let p = self.find_player(num);
        match p {
            Some(_) => return true,
            None => return false,
        }
    }

    pub fn find_player(&mut self, num: &u8) -> Option<&mut Player> {
        //let mut into_iter = self.roster.into_iter();

        for p in self.roster_mut().into_iter() {
            if p.num == *num {
                return Some(p);
            }
        }
        return None
    }

    pub fn starting_lineup(&mut self, players: [u8;6]) -> bool {
        let mut pos: u8 = 1;
        for n in players.iter() {

            let res = self.find_player( &n);
            match res {
                Some(x) => x.to_court(pos),
                None => return false,
            }
            pos += 1;
        }
        return true

    }

    pub fn get_player_pos(&mut self, num: u8) -> u8 {
        let p = self.find_player(&num);
        match p {
            None => return 0,
            Some(x) => {
                return x.pos();
            },
        }
    }

    /// Set player pos to
    pub fn set_player_pos(&mut self, num: u8, pos: u8) -> bool {
        let p = self.find_player(&num);
        match p {
            None => return false,
            Some(x) => {
                x.set_pos(pos);
                return true;
            },
        }
    }

    /// Set the team's players.
    pub fn set_players(&mut self, players: [u8;6]) {
        self.players = players;
    }

    pub fn add_player(&mut self, new_player: Player) -> bool {
        for p in self.roster.iter() {
            if p.num == new_player.num {
                return false;
            }
        }
        self.roster.push(new_player);
        return true;
    }

    pub fn is_player_libero(&mut self, num: u8) -> bool {
        let p: Option<&mut Player> = self.find_player(&num); 
        match p {
            Some(x) => {
                match x.role {
                    Role::Libero => true,
                    _ => false,
                }
            } 
            None => return false,
        }
    }

    pub fn is_player_middle(&mut self, num: u8) -> bool {
        let p: Option<&mut Player> = self.find_player(&num); 
        match p {
            Some(x) => {
                match x.role {
                    Role::Middle => true,
                    _ => false,
                }
            } 
            None => return false,
        }
    }

    pub fn rotate(&mut self){
        for num in *self.players() {
            let p: Option<&mut Player> = self.find_player(&num); 
            match p {
                Some(x) => {
                    //TODO
                    /* 
                    This feature will be implemented with point history
                    if self.is_player_libero(num) && x.pos() == 5 {
                    */
                    x.rotate()
                },
                None => println!("Player not found"),
            }
        }
    }

    pub fn sub(&mut self, on_court: u8, on_bench: u8) -> bool {
        //Player 1 needs to be on court
        //Player 2 needs to be on the bench
        match self.players.iter().find(|&&x| x == on_court) {
            None => return false,
            Some(_) => (),
        }
        if self.get_player_pos(on_bench) != 0 {
            return false;
        }

        //Find two player pos
        let pos1 = self.get_player_pos(on_court);

        self.set_player_pos(on_bench, pos1);
        self.set_player_pos(on_court, 0);

        return true;
    }

    /// Get a reference to the team's roster.
    pub fn roster(&self) -> &Vec::<Player> {
        &self.roster
    }

    /// Get a mutable reference to the team's roster.
    pub fn roster_mut(&mut self) -> &mut Vec::<Player> {
        &mut self.roster
    }

    /// Get a reference to the team's players.
    pub fn players(&self) -> &[u8;6] {
        &self.players
    }
}

impl fmt::Display for Team {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Name : {}\tCoach {}\n\nPlayers :\n", self.name, self.coach)?;
        for p in &self.roster {
            write!(f,"\t{}\n" ,p)?;
        }
        Ok(())
    }
}
