pub mod lib;

use std::error::Error;

use crate::lib::object::matches::*; 
use crate::lib::object::team::Team;
use crate::lib::setup::fn_setup::init;
use crate::lib::draw::main_term::*;
use crate::lib::draw::input_form::*;

use std::fs::File;
use std::io::copy;


fn main(){

    // Init
    let mut lematch = Match::new();
   
    //init(&mut lematch);

    // println!("Test {}" , &lematch.location );
    // Serialize     
    //

    type Term = fn() -> Result<usize, Box<dyn Error>>;

    let term : Term = main_menu_term;
    let choice = match term() {
        Ok(x) => x,
        Err(e) => {
            println!("{:?}", e);
            return ();
        }
    };

    let new_match = init(&mut Match::new());
    //let data = serde_json::to_string(&new_match).unwrap();
    let serialized;
    match choice {
        1 => {
            //start new match option
            //println!("Chosen New match option => display new game term");
            let ret = input_match_term(&mut lematch).expect("Failed");

            lematch.location = &mut ret.0;
            lematch.referee = &mut ret.1;


            println!("Location : {}\tReferee : {}", lematch.location, lematch.referee);
        },
        2 => {
            //Import match data from json with serde
            // Deserialize the match to simulate an import
            //lematch = serde_json::from_str(&data).unwrap();
            ()
        },
        3 => {
            init(&mut lematch);
            serialized = serde_json::to_string(&lematch).unwrap();
            println!("Serde = {}", serialized);
        }
        _ => (),
    };
    //println!("{}", &lematch);
}