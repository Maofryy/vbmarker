use crate::lib::object::matches::*;
use crate::lib::object::player::*;
use crate::lib::object::team::*;



/* 
const LIST_PLAYERS_1 : Vec<Box<Player>> = Vec<Box>[
  .add_player( Player{num: 11, pos: 0, role: Role::Opposite} ); //Sylvain
  .add_player( Player{num: 6,  pos: 0, role: Role::Opposite} ); //Gabi
  .add_player( Player{num: 4,  pos: 0, role: Role::Opposite} ); //Amin
  .add_player( Player{num: 5,  pos: 0, role: Role::Opposite} ); //Erwenn
  .add_player( Player{num: 3,  pos: 0, role: Role::Outside}  ); //Emilien
  .add_player( Player{num: 2,  pos: 0, role: Role::Outside}  ); //Vincent
  .add_player( Player{num: 16, pos: 0, role: Role::Outside}  ); //Cédric
  .add_player( Player{num: 7,  pos: 0, role: Role::Outside}  ); //Coco
  .add_player( Player{num: 10, pos: 0, role: Role::Setter}   );  //Gael
  .add_player( Player{num: 8,  pos: 0, role: Role::Setter}   );  //Mao
  .add_player( Player{num: 1,  pos: 0, role: Role::Setter}   );  //Tof
  .add_player( Player{num: 9,  pos: 0, role: Role::Middle}   );  //Mika
  .add_player( Player{num: 12, pos: 0, role: Role::Middle}   );  //Olivier
  .add_player( Player{num: 13, pos: 0, role: Role::Libero}   );  //Cyril
  .add_player( Player{num: 17, pos: 0, role: Role::Libero}   );  //Thibaud
  .add_player( Player{num: 14, pos: 0, role: Role::Libero}   );  //Stephane
  .add_player( Player{num: 15, pos: 0, role: Role::Libero}   );  //Hugo
];*/

pub fn init(lematch: &mut Match){
    /* 
    let mut players_1 = Vec::<&mut Player>::new();
    for player in LIST_PLAYERS_1.iter_mut() {
        players_1.push(player); 
    } */
    let mut vbba = Team {
        name    : "vbba",
        roster  :  Vec::<Player>::new(),
        coach   : "Tof",
        players  : [0,0,0,0,0,0],
    };

    let mut sm1 = Team {
        name    : "SM1",
        roster  :  Vec::<Player>::new(),
        coach   : "Hugo",
        players  : [0,0,0,0,0,0],
    };

    vbba.add_player(Player{num:11, pos: 0, role: Role::Opposite});
    vbba.add_player( Player{num: 4, pos: 0, role: Role::Opposite} ); //Amin
    vbba.add_player( Player{num: 16,pos: 0, role: Role::Outside}  ); //Cédric
    vbba.add_player( Player{num: 7, pos: 0, role: Role::Outside}  ); //Coco
    vbba.add_player( Player{num: 10,pos: 0, role: Role::Setter}   );  //Gael
    vbba.add_player( Player{num: 12,pos: 0, role: Role::Middle}   );  //Olivier
    vbba.add_player( Player{num: 14,pos: 0, role: Role::Libero}   );  //Stephane
    vbba.add_player( Player{num: 15,pos: 0, role: Role::Libero}   );  //Hugo

    //Pos             I   II  III IV  V   VI
    vbba.starting_lineup([10, 7, 12, 11, 16, 14]);
    // println!("Sm1 = {}", vbba);

    sm1.add_player( Player{num: 6, pos: 0, role: Role::Opposite} ); //Gabi
    sm1.add_player( Player{num: 5,  pos: 0, role: Role::Opposite} ); //Erwenn
    sm1.add_player( Player{num: 3,  pos: 0, role: Role::Outside}  ); //Emilien
    sm1.add_player( Player{num: 2,  pos: 0, role: Role::Outside}  ); //Vincent
    sm1.add_player( Player{num: 8,  pos: 0, role: Role::Setter}   );  //Mao
    sm1.add_player( Player{num: 9,  pos: 0, role: Role::Middle}   );  //Mika
    sm1.add_player( Player{num: 13, pos: 0, role: Role::Libero}   );  //Cyril
    sm1.add_player( Player{num: 17, pos: 0, role: Role::Libero}   );  //Thibaud

    //Pos             I   II  III IV  V   VI
    sm1.starting_lineup([13, 8, 2, 9, 6, 3]);

    // println!("Sm1 = {}", sm1);

    lematch.team1 = vbba;
    lematch.team2 = sm1;
    lematch.referee = "Iris";
    lematch.location = "Perdreau";
}
    