#[allow(dead_code)]
use super::util;
use super::super::object::matches::*;

use serde_json::value::Index;
use util::event::{Event, Events};
use std::{error::Error, io};
use termion::{event::Key, input::MouseTerminal, raw::IntoRawMode, screen::AlternateScreen};
use tui::{Terminal, backend::TermionBackend, layout::{Alignment, Constraint, Direction, Layout}, style::{Color, Modifier, Style}, text::{Span, Spans, Text}, widgets::{Block, Borders, List, ListItem, Paragraph}};

use unicode_width::UnicodeWidthStr;

enum InputMode {
    Normal,
    EditingLocation,
    EditingReferee,
}

/// App holds the state of the application
struct App {
    /// Current value of the input box
    inputa: String,
    location: String,
    referee: String,
    /// Current input mode
    input_mode: InputMode,
}

impl Default for App {
    fn default() -> App {
        App {
            inputa: String::new(),
            location: String::new(),
            referee: String::new(),
            input_mode: InputMode::EditingLocation,
        }
    }
}

pub fn input_match_term(lematch : &mut Match) -> Result<(String, String), Box<dyn Error>> {
    // Terminal initialization
    let stdout = io::stdout().into_raw_mode()?;
    let stdout = MouseTerminal::from(stdout);
    let stdout = AlternateScreen::from(stdout);
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Setup event handlers
    let events = Events::new();

    // Create default app state
    let mut app = App::default();

    loop {
        // Draw UI
        terminal.draw(|f| {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(2)
                .constraints(
                    [
                        Constraint::Ratio(1, 4),
                        Constraint::Ratio(2, 4),
                        Constraint::Ratio(1, 4),
                    ]
                    .as_ref(),
                )
                .split(f.size());

            let title_paragraph = Paragraph::new(vec![
                Spans::from("Setting up match information : ")
                ])
                .style(Style::default().fg(Color::White))
                .alignment(Alignment::Center);
            f.render_widget(title_paragraph, chunks[0]);
            
            let content_chunks =  Layout::default()
            .direction(Direction::Vertical)
            .margin(2)
            .constraints(
                [
                    Constraint::Percentage(20),
                    Constraint::Percentage(40),
                    Constraint::Percentage(40),
                ]
                .as_ref(),
            )
            .margin(2)
            .split(f.size());

            let (msg, style) = match app.input_mode {
                InputMode::Normal => (
                    vec![
                        Span::raw("Press "),
                        Span::styled("q", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to exit, "),
                        Span::styled("e", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to start editing."),
                    ],
                    Style::default().add_modifier(Modifier::RAPID_BLINK),
                ),
                InputMode::EditingLocation => (
                    vec![
                        Span::raw("Press "),
                        Span::styled("Esc", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to stop editing, "),
                        Span::styled("Enter", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to record the message"),
                    ],
                    Style::default(),
                ),
                InputMode::EditingReferee => (
                    vec![
                        Span::raw("Press "),
                        Span::styled("Esc", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to stop editing, "),
                        Span::styled("Enter", Style::default().add_modifier(Modifier::BOLD)),
                        Span::raw(" to record the message"),
                    ],
                    Style::default(),
                ),
            };


            let mut text = Text::from(Spans::from(msg));
            text.patch_style(style);
            let help_message = Paragraph::new(text);
            f.render_widget(help_message, content_chunks[0]);

            let location_input = Paragraph::new(app.location.as_ref())
                .style(match app.input_mode {
                    InputMode::EditingLocation => Style::default().fg(Color::Yellow),
                    _ => Style::default(),
                })
                .block(Block::default().borders(Borders::ALL).title("Location"));
            f.render_widget(location_input, content_chunks[1]);

            let referee_input = Paragraph::new(app.referee.as_ref())
                .style(match app.input_mode {
                    InputMode::EditingReferee => Style::default().fg(Color::Yellow),
                    _ => Style::default(),
                })
                .block(Block::default().borders(Borders::ALL).title("Referee"));
            f.render_widget(referee_input, content_chunks[2]);

            match app.input_mode {
                InputMode::Normal =>
                    // Hide the cursor. `Frame` does this by default, so we don't need to do anything here
                    {}

                InputMode::EditingLocation => {
                    // Make the cursor visible and ask tui-rs to put it at the specified coordinates after rendering
                    f.set_cursor(
                        // Put cursor past the end of the input text
                        content_chunks[1].x + app.location.width() as u16 + 1,
                        // Move one line down, from the border to the input line
                        content_chunks[1].y + 1,
                    )
                },

                InputMode::EditingReferee => {
                    // Make the cursor visible and ask tui-rs to put it at the specified coordinates after rendering
                    f.set_cursor(
                        // Put cursor past the end of the input text
                        content_chunks[2].x + app.referee.width() as u16 + 1,
                        // Move one line down, from the border to the input line
                        content_chunks[2].y + 1,
                    )
                }
            }
            /* 
            let messages: Vec<ListItem> = app
                .messages
                .iter()
                .enumerate()
                .map(|(i, m)| {
                    let content = vec![Spans::from(Span::raw(format!("{}: {}", i, m)))];
                    ListItem::new(content)
                })
                .collect();
            let messages =
                List::new(messages).block(Block::default().borders(Borders::ALL).title("Messages"));
            f.render_widget(messages, chunks[2]);
                */
        })?;

        // Handle input
        match events.next()? {
            Event::Input(input) => match app.input_mode {
                InputMode::Normal => match input {
                    Key::Char('e') => {
                        app.input_mode = InputMode::EditingLocation;
                    }
                    Key::Char('q') => {
                        break;
                    }
                    _ => {}
                },
                InputMode::EditingLocation => match input {
                    Key::Char('\n') => {
                        app.input_mode = InputMode::EditingReferee;
                    }
                    Key::Char(c) => {
                        app.location.push(c);
                    }
                    Key::Backspace => {
                        app.location.pop();
                    }
                    Key::Esc => {
                        app.input_mode = InputMode::Normal;
                    }
                    _ => {}
                },

                InputMode::EditingReferee => match input {
                    Key::Char('\n') => {
                        app.input_mode = InputMode::Normal;
                        // Then call the next term ?? or return with the values and shit
                        return Ok((app.location, app.referee));
                    }
                    Key::Char(c) => {
                        app.referee.push(c);
                    }
                    Key::Backspace => {
                        app.referee.pop();
                    }
                    Key::Esc => {
                        app.input_mode = InputMode::Normal;
                    }
                    _ => {}
                },
            },
            _ => {},
        }
    }
    Ok((app.location, app.referee))
}
